# French translation for contrast.
# Copyright (C) 2020 contrast's COPYRIGHT HOLDER
# This file is distributed under the same license as the contrast package.
#
# Adrien DERACHE <a.d44@tuta.io>, 2020
# Claude Paroz <claude@2xlibre.net, 2020
#
msgid ""
msgstr ""
"Project-Id-Version: contrast master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/design/contrast/issues\n"
"POT-Creation-Date: 2020-02-02 12:40+0000\n"
"PO-Revision-Date: 2020-02-20 18:52+0100\n"
"Last-Translator: Adrien DERACHE <a.d44@tuta.io>\n"
"Language-Team: French <gnomefr@traduc.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Gtranslator 3.34.0\n"

#: data/org.gnome.design.Contrast.appdata.xml.in.in:7
#: data/org.gnome.design.Contrast.desktop.in.in:3 src/main.rs:32
msgid "Contrast"
msgstr "Contraste"

#: data/org.gnome.design.Contrast.appdata.xml.in.in:8
#: data/org.gnome.design.Contrast.desktop.in.in:4
msgid "Check contrast between two colors"
msgstr "Vérifier le contraste entre deux couleurs"

#: data/org.gnome.design.Contrast.appdata.xml.in.in:10
msgid ""
"Contrast checks whether the contrast between two colors meet the APCA "
"requirements."
msgstr ""
"Contraste vérifie si le contraste entre deux couleurs répond aux exigences "
"APCA."

#: data/org.gnome.design.Contrast.appdata.xml.in.in:15
msgid "Main Window"
msgstr "Fenêtre principale"

#: data/org.gnome.design.Contrast.appdata.xml.in.in:43
msgid "Bilal Elmoussaoui"
msgstr "Bilal Elmoussaoui"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.design.Contrast.desktop.in.in:10
msgid "Color;Contrast;GNOME;GTK;"
msgstr "Couleur;Contraste;GNOME;GTK;"

#: data/org.gnome.design.Contrast.gschema.xml.in:6
msgid "Left position of the last opened window"
msgstr "Position gauche de la dernière fenêtre ouverte"

#: data/org.gnome.design.Contrast.gschema.xml.in:10
msgid "Top position of the last opened window"
msgstr "Position supérieure de la dernière fenêtre ouverte"

#: data/org.gnome.design.Contrast.gschema.xml.in:14
msgid "Maximized state of the last opened window"
msgstr "État maximisé de la dernière fenêtre ouverte"

#: data/resources/ui/about_dialog.ui.in:9 data/resources/ui/window.ui.in:7
#: data/resources/ui/window.ui.in:15
msgid "@name-prefix@Contrast"
msgstr "@name-prefix@Contrast"

#: data/resources/ui/about_dialog.ui.in:14
msgid "translator-credits"
msgstr ""
"Adrien DERACHE <a.d44@tuta.io>\n"
"Claude Paroz <claude@2xlibre.net>"

#: data/resources/ui/menu.ui:6
msgid "_Keyboard Shortcuts"
msgstr "_Raccourcis clavier"

#: data/resources/ui/menu.ui:10
msgid "_About Contrast"
msgstr "_À propos de Contraste"

#: data/resources/ui/shortcuts.ui:13
msgctxt "shortcut window"
msgid "General"
msgstr "Général"

#: data/resources/ui/shortcuts.ui:17
msgctxt "shortcut window"
msgid "Reverse Colors"
msgstr "Inverser les couleurs"

#: data/resources/ui/shortcuts.ui:24
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr "Afficher les raccourcis"

#: data/resources/ui/shortcuts.ui:31
msgctxt "shortcut window"
msgid "Quit"
msgstr "Quitter"

#: src/contrast_level.rs:185
msgid "Contrast Ratio: "
msgstr "Taux de contraste :"

#: src/contrast_preview.rs:45
msgid "Nope"
msgstr "Non"

#: src/contrast_preview.rs:46
msgid "This color combination doesn’t have enough contrast to be legible."
msgstr ""
"Cette combinaison de couleurs n’a pas assez de contraste pour être lisible."

#: src/contrast_preview.rs:48
msgid "Not bad"
msgstr "Pas mal"

#: src/contrast_preview.rs:49
msgid "This color combination can work, but only at large text sizes."
msgstr ""
"Cette combinaison de couleurs peut fonctionner, mais seulement pour les "
"textes de grande taille."

#: src/contrast_preview.rs:51
msgid "Pretty good"
msgstr "Plutôt bien"

#: src/contrast_preview.rs:52
msgid "This color combination should work OK in most cases."
msgstr ""
"Cette combinaison de couleurs devrait fonctionner correctement dans la "
"plupart des cas."

#: src/contrast_preview.rs:54
msgid "Awesome"
msgstr "Génial"

#: src/contrast_preview.rs:55
msgid "This color combination has great contrast."
msgstr "Cette combinaison de couleurs présente un fort contraste."

#: src/window.rs:36
msgid "Background Colour"
msgstr "Couleur d’arrière-plan"

#: src/window.rs:37
msgid "Foreground Colour"
msgstr "Couleur de premier plan"
