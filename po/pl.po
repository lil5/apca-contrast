# Polish translation for contrast.
# Copyright © 2019-2020 the contrast authors.
# This file is distributed under the same license as the contrast package.
# Piotr Drąg <piotrdrag@gmail.com>, 2019-2020.
# Aviary.pl <community-poland@mozilla.org>, 2019-2020.
#
msgid ""
msgstr ""
"Project-Id-Version: contrast\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/design/contrast/issues\n"
"POT-Creation-Date: 2021-07-04 17:22+0000\n"
"PO-Revision-Date: 2020-05-24 14:23+0200\n"
"Last-Translator: Piotr Drąg <piotrdrag@gmail.com>\n"
"Language-Team: Polish <community-poland@mozilla.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#: data/org.gnome.design.Contrast.desktop.in.in:3
#: data/org.gnome.design.Contrast.metainfo.xml.in.in:7
#: data/resources/ui/window.ui:14 src/application.rs:133 src/main.rs:26
msgid "Contrast"
msgstr "Kontrast"

#: data/org.gnome.design.Contrast.desktop.in.in:4
#: data/org.gnome.design.Contrast.metainfo.xml.in.in:8 src/application.rs:136
msgid "Check contrast between two colors"
msgstr "Sprawdzanie kontrastu między dwoma kolorami"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.design.Contrast.desktop.in.in:10
msgid "Color;Contrast;GNOME;GTK;"
msgstr "Kolor;Kolory;Color;Colour;Kontrast;Contrast;GNOME;GTK;GTK+;"

#: data/org.gnome.design.Contrast.metainfo.xml.in.in:10
msgid ""
"Contrast checks whether the contrast between two colors meet the APCA "
"requirements."
msgstr ""
"Sprawdzanie, czy kontrast między dwoma kolorami spełnia wymagania APCA."

#: data/org.gnome.design.Contrast.metainfo.xml.in.in:15
msgid "Main Window"
msgstr "Główne okno"

#: data/org.gnome.design.Contrast.metainfo.xml.in.in:49
msgid "Bilal Elmoussaoui"
msgstr "Bilal Elmoussaoui"

#: data/resources/ui/shortcuts.ui:13
msgctxt "shortcut window"
msgid "General"
msgstr "Ogólne"

#: data/resources/ui/shortcuts.ui:17
msgctxt "shortcut window"
msgid "Reverse Colors"
msgstr "Odwrócenie kolorów"

#: data/resources/ui/shortcuts.ui:24
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr "Wyświetlenie skrótów"

#: data/resources/ui/shortcuts.ui:31
msgctxt "shortcut window"
msgid "Quit"
msgstr "Zakończenie działania"

#: data/resources/ui/window.ui:5
msgid "_Keyboard Shortcuts"
msgstr "_Skróty klawiszowe"

#: data/resources/ui/window.ui:9
msgid "_About Contrast"
msgstr "_O programie"

#: data/resources/ui/window.ui:70
msgid "Background Colour"
msgstr "Kolor tła"

#: data/resources/ui/window.ui:81
msgid "Reverse Colours"
msgstr "Odwraca kolory"

#: data/resources/ui/window.ui:90
msgid "Foreground Colour"
msgstr "Kolor tekstu"

#: src/application.rs:140
msgid "translator-credits"
msgstr ""
"Piotr Drąg <piotrdrag@gmail.com>, 2019-2020\n"
"Aviary.pl <community-poland@mozilla.org>, 2019-2020"

#: src/contrast_level.rs:78
msgid "Contrast Ratio∶ "
msgstr "Kontrast: "

#: src/contrast_preview.rs:71
msgid "Nope"
msgstr "Nic z tego"

#: src/contrast_preview.rs:72
msgid "This color combination doesn’t have enough contrast to be legible."
msgstr "To połączenie kolorów nie ma dość kontrastu, aby być czytelne."

#: src/contrast_preview.rs:74
msgid "Not bad"
msgstr "Nie najgorzej"

#: src/contrast_preview.rs:75
msgid "This color combination can work, but only at large text sizes."
msgstr ""
"To połączenie kolorów może być czytelne, ale tylko przy dużych rozmiarach "
"tekstu."

#: src/contrast_preview.rs:77
msgid "Pretty good"
msgstr "Całkiem nieźle"

#: src/contrast_preview.rs:78
msgid "This color combination should work OK in most cases."
msgstr "To połączenie kolorów powinno być czytelne w większości przypadków."

#: src/contrast_preview.rs:80
msgid "Awesome"
msgstr "Super"

#: src/contrast_preview.rs:81
msgid "This color combination has great contrast."
msgstr "To połączenie kolorów ma świetny kontrast."
