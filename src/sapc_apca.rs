struct SortedByContrastLc {
    lc: i32,
    px: [f32; 9],
}

const APCA_SORTED_BY_CONTRAST_LC: [SortedByContrastLc; 15] = [
    SortedByContrastLc {
        lc: 100,
        px: [46.5, 28.0, 18.0, 14.0, 14.0, 14.0, 14.0, 16.0, 18.0],
    },
    SortedByContrastLc {
        lc: 95,
        px: [48.5, 29.0, 20.5, 14.5, 14.0, 14.0, 14.0, 16.0, 18.0],
    },
    SortedByContrastLc {
        lc: 90,
        px: [50.0, 30.0, 21.0, 15.0, 14.5, 14.0, 14.0, 16.0, 18.0],
    },
    SortedByContrastLc {
        lc: 85,
        px: [53.5, 32.0, 22.0, 16.0, 15.0, 14.0, 14.0, 16.0, 18.0],
    },
    SortedByContrastLc {
        lc: 80,
        px: [56.5, 34.0, 23.0, 17.0, 16.0, 14.5, 14.0, 16.0, 18.0],
    },
    SortedByContrastLc {
        lc: 75,
        px: [60.0, 36.0, 24.0, 18.0, 17.0, 15.0, 14.0, 16.0, 18.0],
    },
    SortedByContrastLc {
        lc: 70,
        px: [65.0, 39.0, 27.5, 19.5, 18.0, 16.0, 14.5, 16.0, 18.0],
    },
    SortedByContrastLc {
        lc: 65,
        px: [71.5, 43.0, 30.5, 21.5, 19.5, 17.0, 15.0, 16.0, 18.0],
    },
    SortedByContrastLc {
        lc: 60,
        px: [80.0, 48.0, 34.0, 24.0, 21.0, 18.0, 16.0, 16.0, 18.0],
    },
    SortedByContrastLc {
        lc: 55,
        px: [86.5, 52.0, 37.0, 26.0, 20.5, 18.5, 16.5, 16.0, 18.0],
    },
    SortedByContrastLc {
        lc: 50,
        px: [92.5, 58.0, 41.0, 29.0, 22.0, 20.0, 17.5, 16.5, 18.0],
    },
    SortedByContrastLc {
        lc: 45,
        px: [120.0, 72.0, 51.0, 36.0, 27.0, 23.5, 20.0, 18.0, 18.0],
    },
    SortedByContrastLc {
        lc: 40,
        px: [160.0, 96.0, 68.0, 48.0, 34.0, 29.0, 24.0, 19.0, 18.0],
    },
    SortedByContrastLc {
        lc: 35,
        px: [220.0, 132.0, 93.5, 66.0, 46.5, 40.0, 33.0, 26.5, 20.0],
    },
    SortedByContrastLc {
        lc: 30,
        px: [320.0, 192.0, 135.0, 96.0, 68.0, 58.0, 48.0, 38.5, 29.0],
    },
];

const MAIN_TRC: f32 = 2.4; // 2.4 exponent emulates actual monitor perception

// sRGB coefficients
const RED_CO: f32 = 0.2126729;
const GREEN_CO: f32 = 0.7151522;
const BLUE_CO: f32 = 0.0721750;

const NORM_BG: f32 = 0.56;
const NORM_TXT: f32 = 0.57;
const REV_TXT: f32 = 0.62;
const REV_BG: f32 = 0.65;
// G-4g constants for use with 2.4 exponent

const B_THRESH: f32 = 0.022;
const B_CLAMP: f32 = 1.414;
const SCALE: f32 = 1.14;
const LOW_BW_THRESH: f32 = 0.035991;
const LOW_BW_FACTOR: f32 = 27.7847239587675;
const LOW_BW_OFFSET: f32 = 0.027;
const LOW_CLIP: f32 = 0.001;
const DELTA_Y_MIN: f32 = 0.0005;

//////////  ƒ sRGBtoY()  ///////////////////////////////////////////////////////

// send 8 bit-per-channel integer sRGB (0xFFFFFF)
pub fn rgb_to_y(rgb: (i32, i32, i32)) -> f32 {
    fn simple_exp(chan: i32) -> f32 {
        (chan as f32 / 255.0).powf(MAIN_TRC)
    }
    // linearize r, g, or b then apply coefficients
    // and sum then return the resulting luminance

    RED_CO * simple_exp(rgb.0) + GREEN_CO * simple_exp(rgb.1) + BLUE_CO * simple_exp(rgb.2)
}

//////////  ƒ APCAcontrast()  //////////////////////////////////////////////////

pub fn apca_contrast(txt_color_y: f32, bg_color_y: f32) -> f32 {
    let mut txty = txt_color_y;
    let mut bgy = bg_color_y;

    // send linear Y (luminance) for text and background.
    // IMPORTANT: Do not swap, polarity is important.
    let sapc: f32;

    // TUTORIAL

    // Use Y for text and BG, and soft clamp black,
    // return 0 for very close luminances, determine
    // polarity, and calculate SAPC raw contrast
    // Then scale for easy to remember levels.

    // Note that reverse contrast (white text on black)
    // intentionally returns a negative number
    // Proper polarity is important!

    //////////   BLACK SOFT CLAMP   /////////////////////////////////////////

    // Soft clamps Y for either color if it is near black.
    txty = if txty > B_THRESH { txty } else { txty + (B_THRESH - txty).powf(B_CLAMP) };
    bgy = if bgy > B_THRESH { bgy } else { bgy + (B_THRESH - bgy).powf(B_CLAMP) };

    ///// Return 0 Early for extremely low ∆Y
    if (bgy - txty).abs() < DELTA_Y_MIN {
        return 0.0;
    }

    //////////   APCA/SAPC CONTRAST   ///////////////////////////////////////

    let output_contrast: f32 = if bgy > txty {
        // For normal polarity, black text on white (BoW)

        // Calculate the SAPC contrast value and scale

        sapc = (bgy.powf(NORM_BG) - txty.powf(NORM_TXT)) * SCALE;

        // Low Contrast smooth rollout to prevent polarity reversal
        // and also a low-clip for very low contrasts
        if sapc < LOW_CLIP {
            0.0
        } else if sapc < LOW_BW_THRESH {
            sapc - sapc * LOW_BW_FACTOR * LOW_BW_OFFSET
        } else {
            sapc - LOW_BW_OFFSET
        }
    } else {
        // For reverse polarity, light text on dark (WoB)
        // WoB should always return negative value.

        sapc = (bgy.powf(REV_BG) - txty.powf(REV_TXT)) * SCALE;

        if sapc > -LOW_CLIP {
            0.0
        } else if sapc > -LOW_BW_THRESH {
            sapc - sapc * LOW_BW_FACTOR * LOW_BW_OFFSET
        } else {
            sapc + LOW_BW_OFFSET
        }
    };

    // return Lc (lightness contrast) as a signed numeric value
    // It is permissible to round to the nearest whole number.

    output_contrast * 100.0
} // End APCAcontrast()

////\                             //////////////////////////////////////////////
/////\  END 0.98G4g APCA BLOCK   //////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

pub fn apca_lc_to_pixels(lc: f32) -> Option<[f32; 9]> {
    let lc_rounded = lc.round() as i32;

    let mut prev: Option<i32> = None;
    for sorted_by_contrast in APCA_SORTED_BY_CONTRAST_LC.iter() {
        match prev {
            Some(prev_value) => {
                if lc_rounded < prev_value && lc_rounded >= sorted_by_contrast.lc {
                    return Some(sorted_by_contrast.px);
                }
            }
            _ => {
                if lc_rounded >= sorted_by_contrast.lc {
                    return Some(sorted_by_contrast.px);
                }
            }
        }
        prev = Some(sorted_by_contrast.lc);
    }

    None
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_contrast() {
        struct S {
            txt_color: i32,
            bg_color: i32,
            lc_normal: f32,
            lc_reverse: f32,
        }
        impl S {
            fn new(txt_color: i32, bg_color: i32, lc_normal: f32, lc_reverse: f32) -> S {
                S {
                    txt_color,
                    bg_color,
                    lc_normal,
                    lc_reverse,
                }
            }
        }
        let sut = [S::new(0x1234b0, 0xe9e4d0, 75.6, -78.3), S::new(0x2e2b28, 0xe8cee1, 76.4, -77.2)];

        fn to_y_from_i32(hex_color: i32) -> f32 {
            let r: i32 = (hex_color & 0xFF0000) >> 16;
            let g: i32 = (hex_color & 0x00FF00) >> 8;
            let b: i32 = hex_color & 0x0000FF;

            rgb_to_y((r, g, b))
        }

        sut.iter().for_each(|s| {
            let txt_y = to_y_from_i32(s.txt_color);
            let bg_y = to_y_from_i32(s.bg_color);

            let lc = apca_contrast(txt_y, bg_y);
            assert_eq!((lc * 10.0).round() / 10.0, s.lc_normal);

            let lc_rev = apca_contrast(bg_y, txt_y);
            assert_eq!((lc_rev * 10.0).round() / 10.0, s.lc_reverse);
        });
    }

    #[test]
    fn test_apca_lc_to_pixels() {
        let sut = [(75.0, 3 /*400*/, 18.0), (75.0, 1 /* 200 */, 36.0), (80.9, 5 /* 600 */, 14.5)];

        sut.iter().for_each(|s| {
            let res = apca_lc_to_pixels(s.0);
            assert_eq!(res.unwrap().get(s.1).unwrap(), &s.2);
        })
    }
}
