use gettextrs::gettext;
use gtk::glib;
use gtk::prelude::*;
use gtk::subclass::prelude::*;

mod imp {
    use super::*;

    pub struct ContrastPreview {
        pub head_label: gtk::Label,
        pub head2_label: gtk::Label,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ContrastPreview {
        const NAME: &'static str = "ContrastPreview";
        type ParentType = gtk::Box;
        type Type = super::ContrastPreview;

        fn new() -> Self {
            let head_label = gtk::Label::new(Some("APCA Contrast"));
            let head2_label = gtk::Label::new(None);

            Self { head_label, head2_label }
        }
    }

    impl ObjectImpl for ContrastPreview {
        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            obj.set_orientation(gtk::Orientation::Vertical);
            obj.set_vexpand(true);
            obj.set_valign(gtk::Align::End);
            obj.set_spacing(12);

            self.head2_label.add_css_class("title-1");
            self.head2_label.set_halign(gtk::Align::Center);
            self.head2_label.set_valign(gtk::Align::Start);
            self.head2_label.set_selectable(true);
            self.head2_label.show();

            self.head_label.add_css_class("title-2");
            self.head_label.set_halign(gtk::Align::Center);
            self.head_label.set_valign(gtk::Align::Start);
            self.head_label.show();

            obj.append(&self.head2_label);
            obj.append(&self.head_label);
        }
    }

    impl WidgetImpl for ContrastPreview {}
    impl BoxImpl for ContrastPreview {}
}

glib::wrapper! {
    pub struct ContrastPreview(ObjectSubclass<imp::ContrastPreview>) @extends gtk::Widget, gtk::Box, gtk::Orientable;
}

impl ContrastPreview {
    #[allow(clippy::new_without_default)]
    pub fn new() -> Self {
        glib::Object::new(&[]).unwrap()
    }

    pub fn set_contrast_level(&self, contrast_level: f32) {
        let imp = self.imp();
        let head2_label_txt = &gettext("L<sup>c</sup>");
        let unsigned_contrast_level = if contrast_level.is_sign_positive() { contrast_level } else { contrast_level * -1.0 };

        imp.head2_label.set_markup(format!("{} {:.1}", head2_label_txt, unsigned_contrast_level).as_str());
        // if (0.0..3.0).contains(&contrast_level) {
        //     imp.body_label.set_text(&gettext("This color combination doesn’t have enough contrast to be legible."));
        // } else if (3.0..4.5).contains(&contrast_level) {
        //     imp.head_label.set_text(&gettext("Not bad"));
        //     imp.body_label.set_text(&gettext("This color combination can work, but only at large text sizes."));
        // } else if (4.5..7.0).contains(&contrast_level) {
        //     imp.head_label.set_text(&gettext("Pretty good"));
        //     imp.body_label.set_text(&gettext("This color combination should work OK in most cases."));
        // } else if (7.0..=21.0).contains(&contrast_level) {
        //     imp.head_label.set_text(&gettext("Awesome"));
        //     imp.body_label.set_text(&gettext("This color combination has great contrast."));
        // }
    }
}
