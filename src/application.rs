use super::config;
use super::window::Window;
use gettextrs::gettext;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{
    gdk, gio,
    glib::{self, clone},
};
use gtk_macros::action;
use tracing::info;

pub enum Action {
    ApplyStylesheet(String),
}

mod imp {
    use super::*;
    use adw::prelude::*;
    use adw::subclass::prelude::*;
    use glib::WeakRef;
    use glib::{Receiver, Sender};
    use std::cell::RefCell;

    #[derive(Debug)]
    pub struct Application {
        pub sender: Sender<Action>,
        pub receiver: RefCell<Option<Receiver<Action>>>,
        pub window: RefCell<Option<WeakRef<Window>>>,
        pub provider: gtk::CssProvider,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Application {
        const NAME: &'static str = "Application";
        type ParentType = adw::Application;
        type Type = super::Application;

        fn new() -> Self {
            let (sender, r) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);
            let receiver = RefCell::new(Some(r));

            let provider = gtk::CssProvider::new();
            Self {
                sender,
                receiver,
                window: RefCell::new(None),
                provider,
            }
        }
    }

    impl ObjectImpl for Application {}
    impl ApplicationImpl for Application {
        fn startup(&self, app: &Self::Type) {
            self.parent_startup(app);

            app.style_manager().set_color_scheme(adw::ColorScheme::ForceLight);
        }

        fn activate(&self, app: &Self::Type) {
            if let Some(ref window) = *self.window.borrow() {
                window.upgrade().unwrap().present();
                return;
            }

            // No window available -> we have to create one
            let window = app.create_window();
            window.present();
            self.window.replace(Some(window.downgrade()));
            info!("Created application window.");

            // Setup action channel
            let receiver = self.receiver.borrow_mut().take().unwrap();
            receiver.attach(None, clone!(@weak app => @default-panic, move |action| app.do_action(action)));
        }
    }
    impl GtkApplicationImpl for Application {}
    impl AdwApplicationImpl for Application {}
}

glib::wrapper! {
    pub struct Application(ObjectSubclass<imp::Application>)
        @extends gio::Application, gtk::Application, adw::Application,
        @implements gio::ActionMap, gio::ActionGroup;
}

impl Application {
    pub fn run() {
        info!("Contrast ({})", config::APP_ID);
        info!("Version: {} ({})", config::VERSION, config::PROFILE);
        info!("Datadir: {}", config::PKGDATADIR);

        // Create new GObject and downcast it into Application
        let app = glib::Object::new::<Application>(&[
            ("application-id", &Some(config::APP_ID)),
            ("flags", &gio::ApplicationFlags::empty()),
            ("resource-base-path", &Some("/org/gnome/design/Contrast")),
        ])
        .unwrap();
        app.setup_actions();
        app.setup_accels();

        // Start running gtk::Application
        ApplicationExtManual::run(&app);
    }

    fn create_window(&self) -> Window {
        let imp = self.imp();
        let window = Window::new(imp.sender.clone(), &self.clone());
        if let Some(display) = gdk::Display::default() {
            gtk::StyleContext::add_provider_for_display(&display, &imp.provider, 400);
        }

        window
    }

    fn setup_actions(&self) {
        // Quit
        action!(
            self,
            "quit",
            clone!(@weak self as app => move |_, _| {
                app.quit();
            })
        );

        // About
        action!(
            self,
            "about",
            clone!(@weak self as app => move |_, _| {
                let window = app.active_window().unwrap();
                gtk::AboutDialog::builder()
                    .program_name(&gettext("Contrast"))
                    .modal(true)
                    .version(config::VERSION)
                    .comments(&gettext("Check contrast between two colors"))
                    .website("https://gitlab.gnome.org/World/design/contrast")
                    .authors(vec!["Bilal Elmoussaoui".to_string(), "Zander Brown".to_string(), "Christopher Davis".to_string()])
                    .artists(vec!["Jakub Steiner".to_string(), "Tobias Bernard".to_string()])
                    .translator_credits(&gettext("translator-credits"))
                    .logo_icon_name(config::APP_ID)
                    .license_type(gtk::License::Gpl30)
                    .transient_for(&window)
                    .build()
                    .show();
            })
        );
    }

    fn setup_accels(&self) {
        // Accels
        self.set_accels_for_action("app.quit", &["<primary>q"]);
        self.set_accels_for_action("win.reverse-colors", &["<primary>r"]);
    }

    fn do_action(&self, action: Action) -> glib::Continue {
        match action {
            Action::ApplyStylesheet(text) => self.imp().provider.load_from_data(text.as_bytes()),
        };
        glib::Continue(true)
    }
}
