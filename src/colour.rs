use crate::sapc_apca;
use gtk::gdk;

pub fn calc_contrast_level(bg_color_rgba: &gdk::RGBA, txt_color_rgba: &gdk::RGBA) -> f32 {
    let txt_color = rgba_to_rgb(txt_color_rgba);
    let bg_color = rgba_to_rgb(bg_color_rgba);

    sapc_apca::apca_contrast(sapc_apca::rgb_to_y(txt_color), sapc_apca::rgb_to_y(bg_color))
}

pub fn rgba_to_hex(rgb: &gdk::RGBA) -> String {
    format!("#{}{}{}", to_hex(rgb.red() * 255.0), to_hex(rgb.green() * 255.0), to_hex(rgb.blue() * 255.0))
}

fn rgba_to_rgb(rgb: &gdk::RGBA) -> (i32, i32, i32) {
    let r = rgb.red() * 255.0;
    let g = rgb.green() * 255.0;
    let b = rgb.blue() * 255.0;

    (r.round() as i32, g.round() as i32, b.round() as i32)
}

fn to_hex(n: f32) -> String {
    // Borrowed from https://github.com/emgyrz/colorsys.rs
    let s = format!("{:X}", n.round() as u32);

    s
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_rgba_to_rgb() {
        let rgba = gdk::RGBA::new(18.0 / 255.0, 52.0 / 255.0, 176.0 / 255.0, 1.0);
        let test = rgba_to_rgb(&rgba);

        let ans = (18, 52, 176);

        assert_eq!(test, ans); // aka 1193136
    }

    fn test_num() {
        let test: f32 = 176.022;

        assert_eq!(test.round() as i32, 176);
    }

    #[test]
    fn test_calc_contrast_level() {
        let bg_color_rgba = gdk::RGBA::new(233.0 / 255.0, 228.0 / 255.0, 208.0 / 255.0, 1.0);
        let txt_color_rgba = gdk::RGBA::new(18.0 / 255.0, 52.0 / 255.0, 176.0 / 255.0, 1.0);

        let res = calc_contrast_level(&bg_color_rgba, &txt_color_rgba);

        assert_eq!((res * 10.0).round() / 10.0, 75.6);
    }
}
