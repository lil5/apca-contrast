use super::colour;
use ashpd::{desktop::screenshot, WindowIdentifier};
use gtk::subclass::prelude::*;
use gtk::{
    gdk,
    glib::{self, clone},
    prelude::*,
};
use gtk_macros::spawn;
use std::str::FromStr;

mod imp {
    use super::*;
    use glib::{ParamSpec, ParamSpecBoxed};
    use once_cell::sync::Lazy;
    use std::cell::RefCell;

    #[derive(Debug, gtk::CompositeTemplate)]
    #[template(resource = "/org/gnome/design/Contrast/colour-entry.ui")]
    pub struct ColourEntry {
        pub colour: RefCell<gdk::RGBA>,
    }

    #[gtk::template_callbacks]
    impl ColourEntry {
        #[template_callback]
        fn icon_pressed(&self, pos: gtk::EntryIconPosition, _entry: &gtk::Entry) {
            if pos == gtk::EntryIconPosition::Secondary {
                self.instance().pick_color();
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ColourEntry {
        const NAME: &'static str = "ColourEntry";
        type ParentType = gtk::Entry;
        type Type = super::ColourEntry;

        fn new() -> Self {
            Self {
                colour: RefCell::new(gdk::RGBA::BLACK),
            }
        }

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
            Self::bind_template_callbacks(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }
    impl ObjectImpl for ColourEntry {
        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| vec![ParamSpecBoxed::new("colour", "Colour", "Entry Colour", gdk::RGBA::static_type(), glib::ParamFlags::READWRITE)]);
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _obj: &Self::Type, _id: usize, value: &glib::Value, pspec: &ParamSpec) {
            match pspec.name() {
                "colour" => {
                    let colour = value.get::<Option<gdk::RGBA>>().unwrap().unwrap();
                    self.colour.replace(colour);
                }
                _ => unimplemented!(),
            }
        }

        fn property(&self, _obj: &Self::Type, _id: usize, pspec: &ParamSpec) -> glib::Value {
            match pspec.name() {
                "colour" => self.colour.borrow().to_value(),
                _ => unimplemented!(),
            }
        }

        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);
            obj.set_direction(gtk::TextDirection::Ltr);
            obj.setup_signals();
            obj.set_width_chars(7);
            obj.set_max_width_chars(7);
        }
    }

    impl WidgetImpl for ColourEntry {}
    impl EntryImpl for ColourEntry {}
}

glib::wrapper! {
    pub struct ColourEntry(ObjectSubclass<imp::ColourEntry>) @extends gtk::Widget, gtk::Entry, @implements gtk::Editable;
}

impl ColourEntry {
    #[allow(clippy::new_without_default)]
    pub fn new() -> Self {
        glib::Object::new::<Self>(&[]).expect("Failed to create a ColourEntry")
    }

    pub fn colour(&self) -> gdk::RGBA {
        self.property("colour")
    }

    pub fn set_colour(&self, new_colour: gdk::RGBA) {
        self.set_property("colour", &new_colour);
    }

    fn setup_signals(&self) {
        self.bind_property("colour", self, "text")
            .transform_to(move |_, val| {
                let colour: gdk::RGBA = val.get().unwrap();
                let hex_colour = colour::rgba_to_hex(&colour);
                Some(hex_colour.to_value())
            })
            .transform_from(move |_, val| {
                let text: String = val.get().unwrap();
                gdk::RGBA::from_str(&text).ok().map(|c| c.to_value())
            })
            .flags(glib::BindingFlags::SYNC_CREATE | glib::BindingFlags::BIDIRECTIONAL)
            .build();
    }

    fn pick_color(&self) {
        tracing::info!("Picking a color");
        spawn!(clone!(@weak self as entry => async move {
            let root = entry.root().unwrap();

            let identifier = WindowIdentifier::from_native(&root).await;
            match screenshot::pick_color(&identifier).await {
                Ok(color) => entry.set_colour(color.into()),
                Err(err) => tracing::error!("Failed to pick a color {}", err),
            };
        }));
    }
}
