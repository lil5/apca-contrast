use super::application::{Action, Application};
use super::colour;
use super::colour_entry::ColourEntry;
use super::config::{APP_ID, PROFILE};
use super::contrast_preview::ContrastPreview;
use super::contrast_table::ContrastTable;
use super::sapc_apca::apca_lc_to_pixels;
use glib::clone;
use glib::subclass::prelude::*;
use gtk::{gdk, gio, glib};
use gtk::{prelude::*, CompositeTemplate};
use gtk_macros::action;
use rand::Rng;

// Default colors, randomized whenever you launch the app
// (bg, fg)
type PaletteColor = ((f32, f32, f32), (f32, f32, f32));

const DEFAULT_COLORS: [PaletteColor; 12] = [
    ((228.0, 240.0, 252.0), (29.0, 87.0, 119.0)),
    ((228.0, 220.0, 171.0), (97.0, 53.0, 131.0)),
    ((251.0, 204.0, 231.0), (36.0, 28.0, 140.0)),
    ((230.0, 182.0, 133.0), (99.0, 69.0, 44.0)),
    ((130.0, 108.0, 158.0), (227.0, 222.0, 206.0)),
    ((141.0, 199.0, 149.0), (104.0, 62.0, 38.0)),
    ((160.0, 231.0, 225.0), (72.0, 12.0, 122.0)),
    ((51.0, 14.0, 99.0), (157.0, 233.0, 192.0)),
    ((140.0, 49.0, 82.0), (255.0, 155.0, 123.0)),
    ((174.0, 213.0, 33.0), (89.0, 70.0, 177.0)),
    ((255.0, 112.0, 30.0), (0.0, 43.0, 149.0)),
    ((153.0, 221.0, 238.0), (79.0, 44.0, 18.0)),
];

mod imp {
    use super::*;
    use adw::subclass::prelude::*;
    use glib::subclass;
    use gtk::subclass::prelude::*;
    use std::cell::RefCell;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/org/gnome/design/Contrast/window.ui")]
    pub struct Window {
        #[template_child]
        pub fg_entry: TemplateChild<ColourEntry>,
        #[template_child]
        pub bg_entry: TemplateChild<ColourEntry>,
        #[template_child]
        pub preview: TemplateChild<ContrastPreview>,
        #[template_child]
        pub table: TemplateChild<ContrastTable>,
        pub sender: RefCell<Option<glib::Sender<Action>>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Window {
        const NAME: &'static str = "Window";
        type ParentType = adw::ApplicationWindow;
        type Type = super::Window;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for Window {
        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            if PROFILE == "Devel" {
                obj.add_css_class("devel");
            }
            obj.set_icon_name(Some(APP_ID));
        }
    }

    impl WidgetImpl for Window {}
    impl WindowImpl for Window {}
    impl ApplicationWindowImpl for Window {}
    impl AdwApplicationWindowImpl for Window {}
}

glib::wrapper! {
    pub struct Window(ObjectSubclass<imp::Window>)
        @extends gtk::Widget, gtk::Window, adw::ApplicationWindow,  gtk::ApplicationWindow, gio::ActionMap;
}

impl Window {
    pub fn new(sender: glib::Sender<Action>, app: &Application) -> Self {
        let window = glib::Object::new::<Self>(&[("application", app)]).unwrap();
        window.setup_signals(sender);
        window
    }

    fn colour_changed(&self, bg_colour: gdk::RGBA, fg_colour: gdk::RGBA) {
        let imp = self.imp();

        let contrast_level = colour::calc_contrast_level(&bg_colour, &fg_colour);
        let contrast_level_u = if contrast_level.is_sign_negative() { contrast_level * -1.0 } else { contrast_level };
        let px_by_weight = apca_lc_to_pixels(contrast_level_u);

        imp.preview.set_contrast_level(contrast_level);
        imp.table.set_contrast_level(&px_by_weight);

        let mut css = String::new();
        css.push_str(&format!("@define-color background_color {};\n", bg_colour.to_string()));
        css.push_str(&format!("@define-color foreground_color {};\n", fg_colour.to_string()));

        match px_by_weight {
            Some(px_list) => {
                for i in 2..=7 {
                    let px = px_list.get(i - 2).unwrap();
                    css.push_str(&format!(".table-textarea.table-{}00 {{ font-size: {}px; }}\n", i, px.floor() as i32));
                }
            }
            _ => {}
        }

        imp.sender.borrow().as_ref().unwrap().send(Action::ApplyStylesheet(css)).unwrap();
    }

    fn setup_signals(&self, sender: glib::Sender<Action>) {
        let imp = self.imp();
        imp.sender.borrow_mut().replace(sender);

        let fg_entry = imp.fg_entry.get();
        let bg_entry = imp.bg_entry.get();

        let mut rng = rand::thread_rng();
        let color_index = rng.gen_range(0..DEFAULT_COLORS.len());
        let bg_colour = DEFAULT_COLORS.get(color_index).unwrap().0;
        let fg_colour = DEFAULT_COLORS.get(color_index).unwrap().1;

        let bg_colour = gdk::RGBA::builder().red(bg_colour.0 / 255.0).green(bg_colour.1 / 255.0).blue(bg_colour.2 / 255.0).build();
        let fg_colour = gdk::RGBA::builder().red(fg_colour.0 / 255.0).green(fg_colour.1 / 255.0).blue(fg_colour.2 / 255.0).build();
        self.colour_changed(bg_colour, fg_colour);
        fg_entry.set_colour(fg_colour);
        bg_entry.set_colour(bg_colour);

        let on_entry_changed = clone!(@weak fg_entry, @weak bg_entry, @weak self as window => move |_entry: &ColourEntry| {
            let fg_colour = fg_entry.colour();
            let bg_colour = bg_entry.colour();

            window.colour_changed(bg_colour, fg_colour);
        });

        let bg_handle = bg_entry.connect_changed(on_entry_changed.clone());
        let fg_handle = fg_entry.connect_changed(on_entry_changed);

        action!(
            self,
            "reverse-colors",
            clone!(@weak fg_entry, @weak bg_entry, @weak self as window  => move |_, _| {
                fg_entry.block_signal(&fg_handle);
                bg_entry.block_signal(&bg_handle);
                let fg_colour = fg_entry.colour();
                let bg_colour = bg_entry.colour();
                fg_entry.set_colour(bg_colour);
                bg_entry.set_colour(fg_colour);
                fg_entry.unblock_signal(&fg_handle);
                bg_entry.unblock_signal(&bg_handle);

                window.colour_changed(fg_colour, bg_colour);
            })
        );
    }
}
