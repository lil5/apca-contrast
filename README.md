<a href="https://flathub.org/apps/details/org.gnome.design.Contrast">
<img src="https://flathub.org/assets/badges/flathub-badge-i-en.png" width="190px" />
</a>

# Contrast

<img src="https://gitlab.gnome.org/World/design/contrast/raw/master/data/icons/org.gnome.design.Contrast.svg" width="128" height="128" />
<p>Checks whether the contrast between two colors meet the [SAPC-APCA](https://github.com/Myndex/SAPC-APCA) requirements.</p>

## Screenshots

<div align="center">
![screenshot](data/resources/screenshots/screenshot1.png)
</div>

## Hack on Contrast
To build the development version of Contrast and hack on the code
see the [general guide](https://wiki.gnome.org/Newcomers/BuildProject)
for building GNOME apps with Flatpak and GNOME Builder.

You are expected to follow our [Code of Conduct](https://wiki.gnome.org/Foundation/CodeOfConduct) when participating in project
spaces.
