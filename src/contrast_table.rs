use crate::sapc_apca::apca_lc_to_pixels;
use gettextrs::gettext;
use gtk::glib;
use gtk::prelude::*;
use gtk::subclass::prelude::*;

mod imp {
    use super::*;

    pub struct ContrastTable {
        pub table: gtk::Grid,
        pub td_list: [(gtk::Label, gtk::TextView); 6],
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ContrastTable {
        const NAME: &'static str = "ContrastTable";
        type ParentType = gtk::Box;
        type Type = super::ContrastTable;

        fn new() -> Self {
            let table = gtk::Grid::new();
            let td_list = [
                (gtk::Label::new(None), gtk::TextView::new()),
                (gtk::Label::new(None), gtk::TextView::new()),
                (gtk::Label::new(None), gtk::TextView::new()),
                (gtk::Label::new(None), gtk::TextView::new()),
                (gtk::Label::new(None), gtk::TextView::new()),
                (gtk::Label::new(None), gtk::TextView::new()),
            ];
            Self { table, td_list }
        }
    }

    impl ObjectImpl for ContrastTable {
        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            obj.set_orientation(gtk::Orientation::Vertical);
            obj.set_vexpand(true);
            obj.set_valign(gtk::Align::Center);
            obj.set_spacing(12);

            self.table.set_halign(gtk::Align::Center);
            self.table.set_vexpand(true);
            self.table.set_vexpand_set(true);
            self.table.set_column_spacing(12);
            self.table.set_row_spacing(12);
            self.table.set_column_homogeneous(true);
            self.table.set_css_classes(&["table"]);

            /* table pixel entries */
            let mut i = 2;
            for td in self.td_list.iter() {
                let font_weight = format!("{}00", i);

                let th_px = gtk::Label::new(Some(font_weight.as_str()));
                let px_class = format!("table-{}", font_weight);
                th_px.set_css_classes(&["table-header", px_class.as_str()]);
                th_px.show();

                /* table pixel */
                let td_px = &td.0;
                td_px.set_selectable(true);
                td_px.set_css_classes(&[px_class.as_str()]);
                td_px.show();

                /* table textarea */
                // let size_class = format!("table_")
                let td_textarea = &td.1;
                td_textarea.buffer().set_text("Lorem\nipsum");
                td_textarea.set_css_classes(&["table-textarea", px_class.as_str()]);
                td_textarea.set_editable(false);
                td_textarea.set_focusable(false);
                td_textarea.set_height_request(50);
                td_textarea.set_vexpand(false);
                td_textarea.show();

                self.table.attach(&th_px, i - 1, 0, 1, 1);
                self.table.attach(td_px, i - 1, 1, 1, 1);
                self.table.attach(td_textarea, i - 1, 2, 1, 1);

                i += 1;
            }
            self.table.show();

            obj.append(&self.table);
        }
    }

    impl WidgetImpl for ContrastTable {}
    impl BoxImpl for ContrastTable {}
}

glib::wrapper! {
    pub struct ContrastTable(ObjectSubclass<imp::ContrastTable>) @extends gtk::Widget, gtk::Box, gtk::Orientable;
}

impl ContrastTable {
    #[allow(clippy::new_without_default)]
    pub fn new() -> Self {
        glib::Object::new(&[]).unwrap()
    }

    pub fn set_contrast_level(&self, px_by_weight: &Option<[f32; 9]>) {
        let imp = self.imp();

        match px_by_weight {
            Some(pixels) => {
                let mut weight_index = 1 /* 200 */;
                for td in imp.td_list.iter() {
                    let size = pixels.get(weight_index).unwrap();
                    /* table pixel */
                    /* do stuff to calc minimum size */
                    let td_px = &td.0;
                    let text = format!("{}px", size);
                    td_px.set_text(text.as_str());

                    weight_index += 1;
                }
            }
            _ => {}
        }
    }
}
