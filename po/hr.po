# Croatian translation for contrast.
# Copyright (C) 2020 contrast's COPYRIGHT HOLDER
# This file is distributed under the same license as the contrast package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: contrast master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/design/contrast/"
"issues\n"
"POT-Creation-Date: 2021-07-04 17:22+0000\n"
"PO-Revision-Date: 2021-10-01 15:56+0200\n"
"Last-Translator: gogo <trebelnik2@gmail.com>\n"
"Language-Team: Croatian <hr@li.org>\n"
"Language: hr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Poedit 2.3\n"

#: data/org.gnome.design.Contrast.desktop.in.in:3
#: data/org.gnome.design.Contrast.metainfo.xml.in.in:7
#: data/resources/ui/window.ui:14 src/application.rs:133 src/main.rs:26
msgid "Contrast"
msgstr "Kontrast"

#: data/org.gnome.design.Contrast.desktop.in.in:4
#: data/org.gnome.design.Contrast.metainfo.xml.in.in:8 src/application.rs:136
msgid "Check contrast between two colors"
msgstr "Provjerite kontrast između dvije boje"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.design.Contrast.desktop.in.in:10
msgid "Color;Contrast;GNOME;GTK;"
msgstr "Boja;Kontrast;GNOME;GTK;"

#: data/org.gnome.design.Contrast.metainfo.xml.in.in:10
msgid ""
"Contrast checks whether the contrast between two colors meet the APCA "
"requirements."
msgstr ""
"Kontrast provjerava zadovoljava li kontrast između dvije boje APCA zahtjeve."

#: data/org.gnome.design.Contrast.metainfo.xml.in.in:15
msgid "Main Window"
msgstr "Glavni prozor"

#: data/org.gnome.design.Contrast.metainfo.xml.in.in:49
msgid "Bilal Elmoussaoui"
msgstr "Bilal Elmoussaoui"

#: data/resources/ui/shortcuts.ui:13
msgctxt "shortcut window"
msgid "General"
msgstr "Općenito"

#: data/resources/ui/shortcuts.ui:17
msgctxt "shortcut window"
msgid "Reverse Colors"
msgstr "Obrni boje"

#: data/resources/ui/shortcuts.ui:24
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr "Prikaži prečace"

#: data/resources/ui/shortcuts.ui:31
msgctxt "shortcut window"
msgid "Quit"
msgstr "Zatvori"

#: data/resources/ui/window.ui:5
msgid "_Keyboard Shortcuts"
msgstr "_Prečaci tipkovnice"

#: data/resources/ui/window.ui:9
msgid "_About Contrast"
msgstr "_O Kontrastima"

#: data/resources/ui/window.ui:70
msgid "Background Colour"
msgstr "Boja pozadine"

#: data/resources/ui/window.ui:81
msgid "Reverse Colours"
msgstr "Obrni boje"

#: data/resources/ui/window.ui:90
msgid "Foreground Colour"
msgstr "Prednja boja"

#: src/application.rs:140
msgid "translator-credits"
msgstr ""
"Milo Ivir <mail@milotype.de>, 2020\n"
"Launchpad Contributions:\n"
"  gogo https://launchpad.net/~trebelnik-stefina"

#: src/contrast_level.rs:78
msgid "Contrast Ratio∶ "
msgstr "Omjer kontrasta: "

#: src/contrast_preview.rs:71
msgid "Nope"
msgstr "Ne"

#: src/contrast_preview.rs:72
msgid "This color combination doesn’t have enough contrast to be legible."
msgstr "Ova kombinacija boja nema dovoljno kontrasta da bi tekst bio čitljiv."

#: src/contrast_preview.rs:74
msgid "Not bad"
msgstr "Nije loše"

#: src/contrast_preview.rs:75
msgid "This color combination can work, but only at large text sizes."
msgstr "Ova kombinacija boja je u redu, ali samo za veliki tekst."

#: src/contrast_preview.rs:77
msgid "Pretty good"
msgstr "Dosta dobro"

#: src/contrast_preview.rs:78
msgid "This color combination should work OK in most cases."
msgstr "Ova kombinacija boja je u redu za većinu teksta."

#: src/contrast_preview.rs:80
msgid "Awesome"
msgstr "Super"

#: src/contrast_preview.rs:81
msgid "This color combination has great contrast."
msgstr "Ova kombinacija boja ima veliki kontrast."

#~ msgid "Left position of the last opened window"
#~ msgstr "Lijevi položaj posljednjeg otvorenog prozora"

#~ msgid "Top position of the last opened window"
#~ msgstr "Gornji položaj posljednjeg otvorenog prozora"

#~ msgid "Maximized state of the last opened window"
#~ msgstr "Uvećano stanje posljednjeg otvorenog prozora"
